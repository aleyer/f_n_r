#ifndef SINGLEFILEWIDGET_H
#define SINGLEFILEWIDGET_H

#include <QWidget>
#include "singlereplacementwidget.h"
#include <QSpacerItem>
#include <QFileDialog>
#include <QTextCodec>
#include <QInputDialog>

namespace Ui {
class SingleFileWidget;
}

class SingleFileWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SingleFileWidget(QWidget *parent = nullptr);
    ~SingleFileWidget();
    QVariantList getReplacements();

    QString getFilename() const;
    void setFilename(const QString &value);
    void loadReplacements(QVariantList lst);
    void processFile();
    QByteArray getCodec() const;
    void setCodec(const QByteArray &value);

private:
    Ui::SingleFileWidget *ui;
    QString filename;
    QStringList codecs;
    QByteArray codec;
    QList<SingleReplacementWidget*> widgets;
    QSpacerItem *spacer;
    void addReplacement(QString findStr, QString replaceStr, QByteArray codec);

private slots:
    void addReplacement();
    void removeReplacement();
    void duplicateReplacement();
    void changeFile();

signals:
    void fileChanged(QString newFilename);
    void log(QString entry, QString color);
};

#endif // SINGLEFILEWIDGET_H
