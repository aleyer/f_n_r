#include "frmainwindow.h"
#include "ui_frmainwindow.h"

FRMainWindow::FRMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FRMainWindow)
{
    ui->setupUi(this);

    connect(ui->actionAdd_File,&QAction::triggered,this,&FRMainWindow::addFile);
    connect(ui->actionSave,&QAction::triggered,this,qOverload<>(&FRMainWindow::saveProject));
    connect(ui->actionOpen,&QAction::triggered,this,&FRMainWindow::loadProject);
    connect(ui->actionNew,&QAction::triggered,this,&FRMainWindow::clearProject);
    connect(ui->actionRemove_File,&QAction::triggered,this,&FRMainWindow::removeFile);
    connect(ui->tabWidget,&QTabWidget::currentChanged,this,&FRMainWindow::tabIndexChanged);
    connect(ui->actionProcess,&QAction::triggered,this,&FRMainWindow::process);
    connect(ui->actionAbout,&QAction::triggered,this,&FRMainWindow::showAbout);

    QString lastOpenedProject = settings.value("lastOpenedProjectFilename","").toString();
    if (!lastOpenedProject.isEmpty())
        loadProjectFromFile(lastOpenedProject);
}

FRMainWindow::~FRMainWindow()
{
    delete ui;
}

void FRMainWindow::saveProject(QString filename, QByteArray ba)
{
    QFile f(filename);
    f.open(QFile::WriteOnly);
    f.write(ba);
    f.resize(f.pos());
    f.flush();
    f.close();
}

void FRMainWindow::setLoadedFile(QString filename)
{
    loadedFile = filename;
    QFileInfo inf(filename);
    this->setWindowTitle("F'n'R - "+inf.fileName());
}

void FRMainWindow::filenameForReplacementChanged(QString filename)
{
    QFileInfo inf(filename);
    SingleFileWidget *w = static_cast<SingleFileWidget*>(sender());
    int idx = ui->tabWidget->indexOf(w);
    ui->tabWidget->setTabText(idx,inf.fileName());
}

void FRMainWindow::addFileTab(QString filename, QVariantList replacements, QByteArray codec)
{
    SingleFileWidget *w = new SingleFileWidget(this);
    w->setFilename(filename);
    connect(w,&SingleFileWidget::fileChanged,this,&FRMainWindow::filenameForReplacementChanged);
    connect(w,&SingleFileWidget::log,this,&FRMainWindow::addToLog);
    widgets.append(w);
    QFileInfo info(filename);
    ui->tabWidget->insertTab(ui->tabWidget->count()-1,w,info.fileName());
    ui->tabWidget->setCurrentIndex(ui->tabWidget->count()-2);    
    w->setCodec(codec);
    if (!replacements.isEmpty())
    {
        w->loadReplacements(replacements);
    }
}

void FRMainWindow::loadProject()
{
    QString fnrFilename = QFileDialog::getOpenFileName(this,tr("Open FnR replacements file..."),settings.value("lastOpenedDirForFnrjson","").toString(),"FnR (*.fnrjson)");
    loadProjectFromFile(fnrFilename);
    }

void FRMainWindow::loadProjectFromFile(QString projectFile)
{
    QFileInfo info(projectFile);
    if (!info.exists())
    {
        //QMessageBox::critical(this,tr("Error"),tr("File doesn't exists."));
        return;
    }
    clearProject();
    settings.setValue("lastOpenedDirForFnrjson",info.absoluteDir().absolutePath());
    settings.setValue("lastOpenedProjectFilename",projectFile);
    setLoadedFile(projectFile);
    QFile f(projectFile);
    f.open(QFile::ReadOnly);
    QByteArray ba = f.readAll();
    f.close();
    QJsonDocument doc = QJsonDocument::fromJson(ba);
    QJsonArray ar = doc.array();
    foreach (QVariant v, ar)
    {
        QVariantMap m = v.toMap();
        addFileTab(m["filename"].toString(),m["replacements"].toList(),m.value("encoding","UTF-8").toByteArray());
    }
    //QMessageBox::information(this,tr("Load completed"),tr("Total files in project: ").append(QString::number(ar.count())));

}

void FRMainWindow::clearProject()
{
    ui->logTextEdit->clear();
    loadedFile.clear();
    this->setWindowTitle("F'n'R");
    foreach(SingleFileWidget *w,widgets)
    {
        int idx = ui->tabWidget->indexOf(w);
        widgets.removeAll(w);
        ui->tabWidget->removeTab(idx);
        w->deleteLater();
    }
}

void FRMainWindow::saveProject()
{
    QVariantList entities;
    foreach(SingleFileWidget* w, widgets)
    {
        QVariantMap singleFile;
        QVariantList replacements = w->getReplacements();
        singleFile["replacements"] = replacements;
        singleFile["filename"] = w->getFilename();
        singleFile["encoding"] = w->getCodec();
        entities.append(singleFile);
    }
    QJsonArray ar = QJsonArray::fromVariantList(entities);
    QJsonDocument doc(ar);
    QByteArray ba = doc.toJson(QJsonDocument::Indented);
    if (loadedFile.isEmpty())
    {
        QString filename = QFileDialog::getSaveFileName(this,tr("Save as..."),QCoreApplication::applicationDirPath(),"FnR (*.fnrjson)");
        if (!filename.isEmpty())
            setLoadedFile(filename);
    }
    if (!loadedFile.isEmpty())
        saveProject(loadedFile,ba);

}

void FRMainWindow::process()
{
    ui->logTextEdit->clear();
    ui->tabWidget->setCurrentIndex(ui->tabWidget->count()-1);
    foreach (SingleFileWidget *w, widgets)
    {
        w->processFile();
    }
    addToLog(tr("<b>Processing ended</b>"),"#094f04");
}

void FRMainWindow::addFile()
{
    QString newFileForReplacement = QFileDialog::getOpenFileName(this,"",settings.value("lastOpenedDirForReplacement","").toString());
    QFileInfo info(newFileForReplacement);
    if (!info.exists())
    {
        //QMessageBox::critical(this,tr("Error"),tr("File doesn't exists."));
        return;
    }
    settings.setValue("lastOpenedDirForReplacement",info.absoluteDir().absolutePath());
    addFileTab(newFileForReplacement,QVariantList());
}

void FRMainWindow::removeFile()
{
    QWidget *qw = ui->tabWidget->currentWidget();
    SingleFileWidget *w = static_cast<SingleFileWidget*>(qw);
    int idx = ui->tabWidget->indexOf(w);
    widgets.removeAll(w);
    ui->tabWidget->removeTab(idx);
    w->deleteLater();
}

void FRMainWindow::tabIndexChanged(int idx)
{
    ui->actionRemove_File->setEnabled(ui->tabWidget->count()-idx>1);
}

void FRMainWindow::addToLog(QString entry, QString color)
{
    QString p = "<p style=\"font-size:8pt; color:%color%;\">%text%</p>";
    p.replace("%text%",entry);
    p.replace("%color%",color);
    ui->logTextEdit->append(p);
}

void FRMainWindow::showAbout()
{
    QString aboutText = tr("F'n'R - simple application to find and replace the same strings in the same files with a single mouse click.\n"
            "Written by aleyer\n"
            "Icons: Faenza Icon theme by tiheum");
    QMessageBox::information(this,tr("About F'n'R"),aboutText);
}
