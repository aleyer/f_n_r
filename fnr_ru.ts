<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>FRMainWindow</name>
    <message>
        <location filename="frmainwindow.ui" line="14"/>
        <source>F&apos;n&apos;R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="29"/>
        <source>Log</source>
        <translation>Журнал</translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="35"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:7.8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="76"/>
        <source>Open project</source>
        <translation>Открыть проект</translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="79"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="88"/>
        <source>Clear project</source>
        <translation>Очистить проект</translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="97"/>
        <source>Save project</source>
        <translation>Сохранить проект</translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="100"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="109"/>
        <source>Add File</source>
        <translation>Добавить файл</translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="121"/>
        <source>Remove File</source>
        <translation>Удалить файл</translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="130"/>
        <source>Process</source>
        <translation>Начать обработку</translation>
    </message>
    <message>
        <location filename="frmainwindow.ui" line="139"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="frmainwindow.cpp" line="73"/>
        <source>Open FnR replacements file...</source>
        <translation>Открыть файл замен FnR...</translation>
    </message>
    <message>
        <location filename="frmainwindow.cpp" line="135"/>
        <source>Save as...</source>
        <translation>Сохранить как...</translation>
    </message>
    <message>
        <location filename="frmainwindow.cpp" line="152"/>
        <source>&lt;b&gt;Processing ended&lt;/b&gt;</source>
        <translation>&lt;b&gt;Обработка завершена&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="frmainwindow.cpp" line="193"/>
        <source>F&apos;n&apos;R - simple application to find and replace the same strings in the same files with a single mouse click.
Written by aleyer
Icons: Faenza Icon theme by tiheum</source>
        <oldsource>F&apos;n&apos;R - simple application to find and replace the same strings in the same files over and over again with a single mouse click.
Written by aleyer
Icons: Faenza Icon theme by tiheum</oldsource>
        <translation>F&apos;n&apos;R - простая программа, предназначенная для того, чтобы производить поиск и замену одних и тех же строк в одних и тех же файлах в одно нажатие.
Накодил aleyer
Иконки из темы Faenza авторства tiheum</translation>
    </message>
    <message>
        <location filename="frmainwindow.cpp" line="196"/>
        <source>About F&apos;n&apos;R</source>
        <oldsource>About FnR</oldsource>
        <translation>О программе F&apos;n&apos;R</translation>
    </message>
</context>
<context>
    <name>SingleFileWidget</name>
    <message>
        <location filename="singlefilewidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="singlefilewidget.ui" line="52"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="singlefilewidget.ui" line="90"/>
        <location filename="singlefilewidget.cpp" line="141"/>
        <source>Encoding</source>
        <translation>Кодировка</translation>
    </message>
    <message>
        <location filename="singlefilewidget.ui" line="41"/>
        <location filename="singlefilewidget.ui" line="100"/>
        <source>Change</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <location filename="singlefilewidget.ui" line="66"/>
        <source>Add replacement</source>
        <translation>Добавить замену</translation>
    </message>
    <message>
        <location filename="singlefilewidget.cpp" line="71"/>
        <source>Error opening file: </source>
        <translation>Ошибка открытия файла: </translation>
    </message>
    <message>
        <location filename="singlefilewidget.cpp" line="79"/>
        <source>File is empty: </source>
        <translation>Файл пустой: </translation>
    </message>
    <message>
        <location filename="singlefilewidget.cpp" line="84"/>
        <source>No replacements set for file: </source>
        <translation>Не указаны замены для файла: </translation>
    </message>
    <message>
        <location filename="singlefilewidget.cpp" line="88"/>
        <source>File opened: </source>
        <translation>Файл загружен: </translation>
    </message>
    <message>
        <location filename="singlefilewidget.cpp" line="96"/>
        <source>Skipping empty replacement...&lt;br&gt;</source>
        <translation>Пропуск пустой замены...&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="singlefilewidget.cpp" line="100"/>
        <source>Searching for: </source>
        <translation>Поиск: </translation>
    </message>
    <message>
        <location filename="singlefilewidget.cpp" line="104"/>
        <source> Found %n occurrences, replacing...&lt;br&gt;</source>
        <translation> Найдено %n совпадений, заменяем...&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="singlefilewidget.cpp" line="109"/>
        <source> Found 0 occurrences, skipping...&lt;br&gt;</source>
        <translation> Найдено 0 совпадений, пропускаем...&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="singlefilewidget.cpp" line="141"/>
        <source>Select file encoding:</source>
        <translation>Выберите кодировку файла:</translation>
    </message>
    <message>
        <location filename="singlefilewidget.cpp" line="191"/>
        <source>Open file for replacement...</source>
        <translation>Открыть файл для произведения замен...</translation>
    </message>
</context>
<context>
    <name>SingleReplacementWidget</name>
    <message>
        <location filename="singlereplacementwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="singlereplacementwidget.ui" line="20"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="singlereplacementwidget.ui" line="32"/>
        <source>Replace with</source>
        <translation>Заменить на</translation>
    </message>
    <message>
        <location filename="singlereplacementwidget.ui" line="59"/>
        <source>Duplicate</source>
        <translation>Дублировать</translation>
    </message>
    <message>
        <location filename="singlereplacementwidget.ui" line="70"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="singlereplacementwidget.ui" line="91"/>
        <source>Test</source>
        <translation>Протеститровать</translation>
    </message>
    <message>
        <location filename="singlereplacementwidget.cpp" line="54"/>
        <source>Found % occurrence(s)</source>
        <translation>Найдено % совпадений</translation>
    </message>
</context>
</TS>
