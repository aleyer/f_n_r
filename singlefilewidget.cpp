#include "singlefilewidget.h"
#include "ui_singlefilewidget.h"

SingleFileWidget::SingleFileWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SingleFileWidget)
{
    ui->setupUi(this);

    QList<QByteArray> c_ba = QTextCodec::availableCodecs();
    foreach (QByteArray codec, c_ba)
    {
        codecs.append(codec);
    }
    codecs.sort(Qt::CaseInsensitive);
    codecs.removeAll("UTF-8");
    codecs.insert(0,"UTF-8");

    spacer = new QSpacerItem(10,10,QSizePolicy::Minimum,QSizePolicy::Expanding);
    ui->scrollAreaWidgetContents->layout()->addItem(spacer);
    connect(ui->addReplacementPushButton,&QPushButton::clicked,this,qOverload<>(&SingleFileWidget::addReplacement));
    connect(ui->changeFilePushButton,&QPushButton::clicked,this,&SingleFileWidget::changeFile);
    connect(ui->changeCodecPushButton, &QPushButton::clicked, this, [this]() { this->setCodec(QByteArray()); });
}

SingleFileWidget::~SingleFileWidget()
{
    delete ui;
}

QVariantList SingleFileWidget::getReplacements()
{
    QVariantList lst;
    foreach(SingleReplacementWidget *w, widgets)
    {
        QVariantMap fnr;
        fnr["findStr"] = w->getFindStr();
        fnr["replaceStr"] = w->getReplaceStr();
        lst.append(fnr);
    }

    return lst;
}

QString SingleFileWidget::getFilename() const
{
    return filename;
}

void SingleFileWidget::setFilename(const QString &value)
{
    filename = value;
    ui->filenameLineEdit->setText(value);

}

void SingleFileWidget::loadReplacements(QVariantList lst)
{
    foreach(QVariant v, lst)
    {
        QVariantMap fnr = v.toMap();
        addReplacement(fnr["findStr"].toString(),fnr["replaceStr"].toString(),codec);
    }
}

void SingleFileWidget::processFile()
{
    QFile f(filename);
    if (!f.open(QFile::ReadWrite))
    {
        emit log(tr("Error opening file: ").append(filename),"#4f0303");
        return;
    }
    QByteArray ba = f.readAll();
    QTextCodec *tc = QTextCodec::codecForName(codec);
    QString contents = tc->toUnicode(ba);
    if (contents.isEmpty())
    {
        emit log(tr("File is empty: ").append(filename),"#4f0303");
        return;
    }
    if (widgets.isEmpty())
    {
        emit log(tr("No replacements set for file: ").append(filename),"#4f0303");
        return;
    }
    QString logStr;
    logStr.append(tr("File opened: ").append(filename));
    logStr.append("<br>");
    foreach (SingleReplacementWidget *w, widgets)
    {
        QString findStr = w->getFindStr();
        QString replaceStr = w->getReplaceStr();
        if (findStr.isEmpty())
        {
            logStr.append(tr("Skipping empty replacement...<br>"));
        }
        else
        {
            logStr.append(tr("Searching for: ").append(findStr.toHtmlEscaped()));
            int oc = contents.count(findStr);
            if (oc>0)
            {
                logStr.append(tr(" Found %n occurrences, replacing...<br>").replace("%n",QString::number(oc)));
                contents.replace(findStr,replaceStr);
            }
            else
            {
                logStr.append(tr(" Found 0 occurrences, skipping...<br>"));
            }
        }
    }
    emit log(logStr,"#111111");
    f.seek(0);
    ba = tc->fromUnicode(contents);
    f.write(ba);
    f.resize(f.pos());
    f.flush();
    f.close();

}

QByteArray SingleFileWidget::getCodec() const
{
    return codec;
}

void SingleFileWidget::setCodec(const QByteArray &value)
{
    if (!value.isEmpty())
    {
        codec = value;
        ui->codecLineEdit->setText(value);
        foreach(SingleReplacementWidget *w, widgets)
        {
            w->setCodec(value);
        }
        return;
    }
    bool ok;
    QString selection = QInputDialog::getItem(this,tr("Encoding"),tr("Select file encoding:"),codecs,0,false,&ok);
    if (!ok)
    {
        setCodec("UTF-8");
    }
    else
    {
        setCodec(selection.toUtf8());
    }
}

void SingleFileWidget::addReplacement(QString findStr, QString replaceStr, QByteArray codec)
{
    SingleReplacementWidget *w = new SingleReplacementWidget(this);
    w->setFilename(filename);
    w->setFindStr(findStr);
    w->setReplaceStr(replaceStr);
    w->setCodec(codec);
    widgets.append(w);
    ui->scrollAreaWidgetContents->layout()->addWidget(w);
    ui->scrollAreaWidgetContents->layout()->removeItem(spacer);
    ui->scrollAreaWidgetContents->layout()->addItem(spacer);
    connect(w,&SingleReplacementWidget::removeMe,this,&SingleFileWidget::removeReplacement);
    connect(w,&SingleReplacementWidget::duplicateMe,this,&SingleFileWidget::duplicateReplacement);
    connect(this,&SingleFileWidget::fileChanged,w,&SingleReplacementWidget::setFilename);
}

void SingleFileWidget::addReplacement()
{
    addReplacement(QString(),QString(),codec);
}

void SingleFileWidget::removeReplacement()
{
    SingleReplacementWidget *w = static_cast<SingleReplacementWidget*>(sender());
    widgets.removeAll(w);
    ui->scrollAreaWidgetContents->layout()->removeWidget(w);
    w->deleteLater();
}

void SingleFileWidget::duplicateReplacement()
{
    SingleReplacementWidget *w = static_cast<SingleReplacementWidget*>(sender());
    addReplacement(w->getFindStr(),w->getReplaceStr(),codec);
}

void SingleFileWidget::changeFile()
{
    QFileInfo oldInfo(filename);
    QString dir = oldInfo.absoluteDir().absolutePath();
    QString newFileForReplacement = QFileDialog::getOpenFileName(this,tr("Open file for replacement..."), dir);
    QFileInfo info(newFileForReplacement);
    if (!info.exists())
    {
        return;
    }
    setFilename(newFileForReplacement);
    emit fileChanged(filename);
}

