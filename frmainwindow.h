#ifndef FRMAINWINDOW_H
#define FRMAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include "singlefilewidget.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QAction>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>

namespace Ui {
class FRMainWindow;
}

class FRMainWindow : public QMainWindow
{
    Q_OBJECT

private:
    QSettings settings;
    QList<SingleFileWidget*> widgets;
    QString loadedFile;

public:
    explicit FRMainWindow(QWidget *parent = nullptr);
    ~FRMainWindow();

private:
    Ui::FRMainWindow *ui;
    void saveProject(QString filename, QByteArray ba);
    void setLoadedFile(QString filename);
    void filenameForReplacementChanged(QString filename);
    void addFileTab(QString filename, QVariantList replacements, QByteArray codec = QByteArray());

private slots:
    void loadProject();
    void loadProjectFromFile(QString projectFile);
    void clearProject();
    void saveProject();
    void process();
    void addFile();
    void removeFile();
    void tabIndexChanged(int idx);
    void addToLog(QString entry, QString color);
    void showAbout();
};

#endif // FRMAINWINDOW_H
