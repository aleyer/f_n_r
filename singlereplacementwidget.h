#ifndef SINGLEREPLACEMENTWIDGET_H
#define SINGLEREPLACEMENTWIDGET_H

#include <QWidget>
#include <QFile>
#include <QTextCodec>

namespace Ui {
class SingleReplacementWidget;
}

class SingleReplacementWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SingleReplacementWidget(QWidget *parent = nullptr);
    ~SingleReplacementWidget();

    QString getFindStr() const;
    void setFindStr(const QString &value);

    QString getReplaceStr() const;
    void setReplaceStr(const QString &value);
    void setFilename(const QString &value);

    QByteArray getCodec() const;
    void setCodec(const QByteArray &value);

private:
    Ui::SingleReplacementWidget *ui;
    QString filename;
    QByteArray codec;

private slots:
    void runTest();

signals:
    void removeMe();
    void duplicateMe();
};

#endif // SINGLEREPLACEMENTWIDGET_H
