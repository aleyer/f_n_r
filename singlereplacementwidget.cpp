#include "singlereplacementwidget.h"
#include "ui_singlereplacementwidget.h"

SingleReplacementWidget::SingleReplacementWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SingleReplacementWidget)
{
    ui->setupUi(this);
    connect(ui->testPushButton,&QPushButton::clicked,this,&SingleReplacementWidget::runTest);
    connect(ui->findLineEdit,&QLineEdit::textChanged,ui->testResultLabel,&QLabel::clear);
    connect(ui->removePushButton, &QPushButton::clicked,this,&SingleReplacementWidget::removeMe);
    connect(ui->duplicatePushButton, &QPushButton::clicked,this,&SingleReplacementWidget::duplicateMe);
}

SingleReplacementWidget::~SingleReplacementWidget()
{
    delete ui;
}

QString SingleReplacementWidget::getReplaceStr() const
{
    return ui->replaceLineEdit->text();
}

void SingleReplacementWidget::setReplaceStr(const QString &value)
{
    ui->replaceLineEdit->setText(value);
}

void SingleReplacementWidget::setFilename(const QString &value)
{
    filename = value;
}

QByteArray SingleReplacementWidget::getCodec() const
{
    return codec;
}

void SingleReplacementWidget::setCodec(const QByteArray &value)
{
    codec = value;
}

void SingleReplacementWidget::runTest()
{
    QFile f(filename);
    f.open(QFile::ReadOnly);
    QByteArray ba = f.readAll();
    QTextCodec *tc = QTextCodec::codecForName(codec);
    QString str = tc->toUnicode(ba);
    f.close();
    int count = str.count(ui->findLineEdit->text());
    ui->testResultLabel->setText(tr("Found % occurrence(s)").replace("%",QString::number(count)));

}

QString SingleReplacementWidget::getFindStr() const
{
    return ui->findLineEdit->text();
}

void SingleReplacementWidget::setFindStr(const QString &value)
{
    ui->findLineEdit->setText(value);
}
