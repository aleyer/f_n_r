#include "frmainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QString translationsPath(QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    QLocale locale = QLocale::system();
    QTranslator qtTranslator;
    if (qtTranslator.load(locale, "qt", "_", translationsPath))
        a.installTranslator(&qtTranslator);
    QTranslator qtBaseTranslator;
    if (qtBaseTranslator.load(locale, "qtbase", "_", translationsPath))
        a.installTranslator(&qtBaseTranslator);
    QTranslator fnrTranslator;
    fnrTranslator.load("fnr_ru",translationsPath);
    a.installTranslator(&fnrTranslator);
    QCoreApplication::setOrganizationName("aleyer");
    QCoreApplication::setApplicationName("FnR");
    FRMainWindow w;
    w.show();

    return a.exec();
}
